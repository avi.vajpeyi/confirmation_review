"""
Make a plot of the default prior's mass ranges.
"""
import glob
import logging
import math
import os
import re
from typing import List

import bilby_pipe
import matplotlib.font_manager
import numpy as np
import pandas as pd
import seaborn as sns
from bilby.core.prior import PriorDict
from bilby.gw.conversion import (chirp_mass_and_mass_ratio_to_total_mass,
                                 component_masses_to_chirp_mass,
                                 component_masses_to_mass_ratio,
                                 total_mass_and_mass_ratio_to_component_masses)
from matplotlib.patches import Polygon
from tqdm import tqdm

# fmt: off
import matplotlib  # isort:skip noqa

matplotlib.use("agg")  # noqa
import matplotlib.pyplot as plt  # isort:skip noqa

# fmt: on

logging.basicConfig(level=logging.INFO)

RESOLUTION = 2000

EVENT = "event"
MASS_1 = "mass_1"
MASS_2 = "mass_2"
MASS_1_RANGE = dict(min=1, max=500)
MASS_2_RANGE = dict(min=1, max=500)
MASS_GAP = dict(min=60, max=120)
MASS_1_UNC = "mass_1_unc"
MASS_2_UNC = "mass_2_unc"
MASS_REMNANT = "mass_remnant"
MASS_REMNANT_UNC = "mass_remnant_unc"
CHIRP_MASS = "chirp_mass"
MASS_RATIO = "mass_ratio"
TOTAL_MASS = "total_mass"
COMPONENT_MASS = "component_mass"

TRANSPARENT = (1, 1, 1, 0)

SMALL_SIZE = 22
MEDIUM_SIZE = 26
LARGE_SIZE = 30

rotated_labels = []


def get_colors(num_colors, alpha):
    """Get a list of colorblind colors."""
    cs = sns.color_palette(palette="colorblind", n_colors=num_colors)
    cs = [list(c) for c in cs]
    for i in range(len(cs)):
        cs[i].append(alpha)
    return cs


COLORS = get_colors(num_colors=3, alpha=0.7)


def adjust_matplotlib_settings():
    """Wraps matplotlib settings in a function."""
    plt.grid(True)
    font = {"family": "sans-serif", "sans-serif": ["Scope One"], "size": MEDIUM_SIZE}
    matplotlib.rc("font", **font)

    plt.rc("font", size=MEDIUM_SIZE)  # controls default text sizes
    plt.rc("axes", titlesize=SMALL_SIZE)  # fontsize of the axes title
    plt.rc("axes", labelsize=LARGE_SIZE)  # fontsize of the x and y labels
    plt.rc("xtick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc("ytick", labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc("legend", fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc("figure", titlesize=LARGE_SIZE)  # fontsize of the figure title

    plt.rc("axes", linewidth=2)
    # set tick length
    plt.rc("xtick.major", size=10)
    plt.rc("ytick.major", size=10)
    plt.rc("xtick.minor", size=5)
    plt.rc("ytick.minor", size=5)
    # leave minor ticks on
    plt.rc("xtick.minor", visible=True)
    plt.rc("ytick.minor", visible=True)
    # mirroring axes
    plt.rc("xtick", top=True)
    plt.rc("ytick", right=True)
    plt.rc("xtick", direction="in")
    plt.rc("ytick", direction="in")
    plt.figure(figsize=(8, 6))


@np.vectorize
def correct_ordering_of_component_masses(mass_1, mass_2):
    """Ensure mass 1 always greater than mass 2."""
    if mass_1 > mass_2:
        return mass_1, mass_2
    else:
        return mass_2, mass_1


def get_masses_sample_from_prior(prior, num_samples):
    """Sample chirp mass and q from the prior space and returns all mass parameters.

    Parameters
    ----------
    prior: prior dictionary with `chirp_mass' and `mass_ratio'.
    num_samples: number of samples to be taken from the prior file.

    Returns
    -------
    Pandas dataframe with the samples.

    """
    # vectorize some conversion functions
    total_mass_conversion = np.vectorize(chirp_mass_and_mass_ratio_to_total_mass)
    component_mass_conversion = np.vectorize(
        total_mass_and_mass_ratio_to_component_masses
    )

    # sample prior space and convert to all mass params
    samples = pd.DataFrame(prior.sample(size=num_samples))
    chirp_mass = samples[CHIRP_MASS].values
    mass_ratio = samples[MASS_RATIO].values
    total_mass = total_mass_conversion(chirp_mass=chirp_mass, mass_ratio=mass_ratio)
    mass_1, mass_2 = component_mass_conversion(
        total_mass=total_mass, mass_ratio=mass_ratio
    )
    mass_1, mass_2 = correct_ordering_of_component_masses(mass_1, mass_2)

    mass_dataframe = pd.DataFrame(
        {
            MASS_1: mass_1,
            MASS_2: mass_2,
            CHIRP_MASS: chirp_mass,
            MASS_RATIO: mass_ratio,
            TOTAL_MASS: total_mass,
        }
    )
    return mass_dataframe


def get_gwtc1_masses():
    df = pd.read_csv("gwtc1-masses.csv")
    data = {
        EVENT: df.event.values,
        MASS_1: df.mass_1.values,
        MASS_2: df.mass_2.values,
        MASS_REMNANT: df.mass_remnant.values,
        MASS_1_UNC: np.vstack((-df.mass_1_n_unc.values, df.mass_1_p_unc.values)),
        MASS_2_UNC: np.vstack((-df.mass_2_n_unc.values, df.mass_2_p_unc.values)),
        MASS_REMNANT_UNC: np.vstack(
            (-df.mass_remnant_n_unc.values, df.mass_remnant_p_unc.values)
        ),
    }
    return data


def get_o2_foreground_triggers():
    df = pd.read_csv("o2-foreground-masses.csv")
    data = {MASS_1: df.mass_1.values, MASS_2: df.mass_2.values}
    return data


def get_ias_triggers():
    df = pd.read_csv("ias-masses.csv")
    data = {MASS_1: df.mass_1.values, MASS_2: df.mass_2.values}
    return data


def get_template_bank():
    df = pd.read_csv("template_bank.csv")
    data = {
        MASS_1: df.mass_1.values,
        MASS_2: df.mass_2.values,
        TOTAL_MASS: df.mass_total.values,
    }
    return data


def get_prior_files(local_files=True) -> List[str]:
    """Return a list of paths to prior files in bilby_pipe's data files."""
    if local_files:
        paths = glob.glob("*.prior")
    else:  # get bilby_pipe's prior files
        module_root, _ = os.path.split(bilby_pipe.__file__)
        prior_path = os.path.join(module_root, r"data_files/*[0-9]s.prior")
        paths = glob.glob(prior_path)
        paths.sort(key=lambda f: int(re.sub(r"\D", "", f)))
    return paths


def load_priors(prior_files):
    """Return a dict of the {prior_file_name: PriorDict}."""
    loaded_priors = dict()
    for prior_file in prior_files:
        prior_file_basename = os.path.basename(prior_file)
        prior = PriorDict(filename=prior_file)
        loaded_priors.update({prior_file_basename: prior})
    return loaded_priors


def get_m1m2_grid(m1_range, m2_range, prior, resolution):
    """Generate a grid of m1 and m2 and mark point if in prior space."""
    xs = np.linspace(m1_range[0], m1_range[1], resolution)
    ys = np.linspace(m2_range[0], m2_range[1], resolution)[::-1]
    m1, m2 = np.meshgrid(xs, ys)
    in_prior = np.zeros(shape=(len(xs), len(ys)))
    for nrx, loop_m1 in enumerate(tqdm(xs)):
        for nry, loop_m2 in enumerate(ys):
            if loop_m2 > loop_m1:
                pass  # by definition, we choose only m2 smaller than m1
            if loop_m2 < loop_m1:
                mc = component_masses_to_chirp_mass(loop_m1, loop_m2)
                q = component_masses_to_mass_ratio(loop_m1, loop_m2)
                tot_m = loop_m2 + loop_m1
                if (
                    prior.prob(
                        dict(
                            chirp_mass=mc,
                            mass_ratio=q,
                            total_mass=tot_m,
                            mass_1=loop_m1,
                            mass_2=loop_m2,
                        )
                    )
                    > 0
                ):
                    in_prior[nry][nrx] = 1
    return m1, m2, in_prior


def get_line_slope(xdata, ydata, ax, debugging=False):
    if debugging:
        ax.plot(xdata, ydata, "--", color="green")

    # find the slope
    p1, p2 = np.array([xdata[0], ydata[0]]), np.array([xdata[-1], ydata[-1]])

    # get the line's data transform in pixels
    sp1 = ax.transData.transform_point(p1)
    sp2 = ax.transData.transform_point(p2)
    rise, run = (sp2[1] - sp1[1]), (sp2[0] - sp1[0])
    slope_degrees = math.degrees(math.atan(rise / run))

    return dict(
        xy=(np.average(xdata) - 20, np.average(ydata) - 20), rotation=slope_degrees
    )


def add_prior_contours(ax, prior_files_dict, x_key, y_key, get_grid, resolution):
    """Generate prior plot with x and y axis being some combination of mass params."""
    for i, (file, pri) in enumerate(prior_files_dict.items()):
        logging.info(f"Creating bounds for {file}")
        mass_data = get_masses_sample_from_prior(pri, RESOLUTION)
        x_vals = mass_data[x_key].values
        y_vals = mass_data[y_key].values
        x_range = [min(0.8 * x_vals), max(x_vals * 1.2)]
        y_range = [min(0.8 * y_vals), max(y_vals * 1.2)]
        x, y, z = get_grid(x_range, y_range, pri, resolution)
        assert np.count_nonzero(z) > 0, f"None of the sampled values are in {file}"
        ax.contour(
            x, y, z, levels=[0], colors="k", linewidths=2, linestyles="--", zorder=5
        )
        ax.annotate(
            "Search Space",
            xytext=(-80, -40),
            textcoords="offset points",
            color="black",
            horizontalalignment="left",
            verticalalignment="bottom",
            **get_line_slope([10, 200], [10, 200], ax),
        )

    return ax


def add_legend(ax):
    """Set up legened for the bank plot.

    :param ax:
    :return:
    """
    bank_patch = ax.scatter([], [], label="Template Bank", marker="o", color="plum")
    handles = [bank_patch]
    ax.legend(handles=handles, fontsize="large")
    return ax


def add_pycbc_template_bank(ax):
    template_bank = get_template_bank()
    ax.scatter(
        x=template_bank[MASS_1],
        y=template_bank[MASS_2],
        color="plum",
        marker=".",
        s=0.5,
        alpha=0.1,
        zorder=1,
    )
    return ax


def add_mass_gap_to_plot(ax):
    xy = [
        (MASS_GAP["min"], MASS_2_RANGE["min"]),
        (MASS_GAP["max"], MASS_2_RANGE["min"]),
        (MASS_GAP["max"], MASS_GAP["max"]),
        (MASS_1_RANGE["min"], MASS_GAP["max"]),
        (MASS_1_RANGE["min"], MASS_GAP["min"]),
        (MASS_GAP["min"], MASS_GAP["min"]),
        (MASS_GAP["min"], MASS_2_RANGE["min"]),
    ]
    polygon = Polygon(
        xy, closed=True, facecolor="0.9", edgecolor="0.5", alpha=0.6, zorder=0
    )
    ax.annotate(
        "PISN Mass Gap",
        xy=(MASS_1_RANGE["min"], MASS_GAP["max"]),
        xycoords="data",
        xytext=(5, 0),
        textcoords="offset points",
        color="gray",
        horizontalalignment="left",
        verticalalignment="bottom",
    )
    ax.add_patch(polygon)
    return ax


def add_gwtc1_black_holes(ax, c, annotate_gw170729=False):
    gwtc1_data = get_gwtc1_masses()
    ax.errorbar(
        x=gwtc1_data[MASS_1],
        y=gwtc1_data[MASS_2],
        xerr=gwtc1_data[MASS_1_UNC],
        yerr=gwtc1_data[MASS_2_UNC],
        fmt="o",
        color=c,
        ecolor=c,
        alpha=0.8,
        elinewidth=2,
        capsize=3,
        label="LIGO Black Holes",
        zorder=2,
    )
    if annotate_gw170729:
        data = pd.DataFrame(
            {
                EVENT: gwtc1_data[EVENT],
                MASS_1: gwtc1_data[MASS_1],
                MASS_2: gwtc1_data[MASS_2],
            }
        )
        data = data[data[EVENT] == "GW170729"]
        ax.annotate(
            "Gw170729",
            xy=(data[MASS_1].values[0], data[MASS_2].values[0]),
            xycoords="data",
            xytext=(-100, 0),
            textcoords="offset points",
            arrowprops=dict(facecolor=c, edgecolor=c, shrink=0.05, zorder=6),
            horizontalalignment="right",
            verticalalignment="center",
            color=c,
        )
    return ax


def add_o2_foreground_triggers(ax, c):
    data = get_o2_foreground_triggers()
    ax.scatter(
        x=data[MASS_1],
        y=data[MASS_2],
        label="O2 High Mass Candidates",
        color=c,
        zorder=3,
    )
    return ax


def add_ias_triggers(ax, c):
    data = get_ias_triggers()
    ax.scatter(
        x=data[MASS_1], y=data[MASS_2], label="IAS Black Holes", color=c, zorder=4
    )
    return ax


def setup_m1m2_figure():
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    ax.tick_params(which="both", width=1)
    ax.tick_params(which="major", length=8)
    ax.tick_params(which="minor", length=5)
    ax.set_ylabel(r"Secondary Mass [M$_{\odot}$]")
    ax.set_xlabel(r"Primary Mass [M$_{\odot}$]")
    ax.set_yscale("log")
    ax.set_xscale("log")
    ax.set_xlim(MASS_1_RANGE["min"], MASS_1_RANGE["max"])
    ax.set_ylim(MASS_2_RANGE["min"], MASS_2_RANGE["max"])
    ax.grid(True, which="major")
    ax.spines["bottom"].set_color("k")
    ax.spines["top"].set_color("k")
    ax.spines["right"].set_color("k")
    ax.spines["left"].set_color("k")
    ax = add_legend(ax)
    return fig, ax


def add_legend_and_save(filename, fig, ax):
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(
        handles, labels, loc="upper right", bbox_to_anchor=(1.75, 1), frameon=False
    )
    fig.savefig(filename, bbox_extra_artists=[lgd], bbox_inches="tight")


def make_m1m2_plot(resolution):
    logging.info(f"Creating prior-plot for {MASS_1} and {MASS_2}")
    priors = load_priors(get_prior_files())

    fig, ax = setup_m1m2_figure()
    ax = add_mass_gap_to_plot(ax)
    ax = add_pycbc_template_bank(ax)
    add_legend_and_save("bank/empty_bank.png", fig, ax)

    ax = add_gwtc1_black_holes(ax, c=COLORS[0])
    add_legend_and_save("bank/O2_ligo_detections.png", fig, ax)

    ax = add_o2_foreground_triggers(ax, c=COLORS[1])
    add_legend_and_save("bank/O2_ligo_triggers.png", fig, ax)

    ax = add_ias_triggers(ax, c=COLORS[2])
    add_legend_and_save("bank/ias_triggers.png", fig, ax)

    ax = add_prior_contours(
        ax, priors, MASS_1, MASS_2, get_grid=get_m1m2_grid, resolution=resolution
    )
    add_legend_and_save("bank/search_space.png", fig, ax)

    fig, ax = setup_m1m2_figure()
    ax = add_mass_gap_to_plot(ax)
    ax = add_pycbc_template_bank(ax)
    ax = add_gwtc1_black_holes(ax, c=COLORS[0], annotate_gw170729=True)
    add_legend_and_save("bank/O2_ligo_detections_gw170729.png", fig, ax)


def main():
    adjust_matplotlib_settings()
    make_m1m2_plot(resolution=RESOLUTION)


if __name__ == "__main__":
    main()
