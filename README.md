![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---


---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Initial Review](#initial-review)
  - [Bookdown](#bookdown)
  - [Installations](#installations)
  - [Contributing to repo](#contributing-to-repo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# [Confirmation Review](https://avi.vajpeyi.docs.ligo.org/confirmation_review)
Document made for Monash's confirmation review. The confirmation project review is 
the second formal review of a graduate research student's academic performance in the Faculty of Science ([source](https://www.monash.edu/science/current-students/your-candidature)).

## Bookdown 
This document is complied with [boookdown](https://bookdown.org/yihui/bookdown/).

## Installations
To build the document (pdf and website) locally, download R, Rstudio. Then install the 'bookdown' and 'TinyTex' packages in Rstudio. Following this, the documents can be build using the `build book` command, or by following instructions found [here]( https://bookdown.org/yihui/bookdown/build-the-book.html).


## Contributing to repo
run
```
$ pre-commit install && pre-commit install -t pre-push
```
